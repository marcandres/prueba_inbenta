<?php

namespace App;

class Tweet
{
    public $text = '';
    public $user_name = '';

    function __construct($tweet)
   {
      $this->text = $tweet->text;
      $this->user_name = $tweet->user->name;
   }

   public function text(){
   		return $this->text;
   }

   public function user_name(){
   		return $this->user_name;
   }
}

?>