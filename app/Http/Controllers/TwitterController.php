<?php namespace App\Http\Controllers;

use Session;
use TwitterAPIExchange;
use Mail;
use App\Tweet;
use Input;
use Request;
use Response;
use Redirect;

class TwitterController extends Controller{

    public function getTweet(){

        $text_input = $_POST['text'];
        $email = $_POST['email'];
        //echo json_encode($email); die;
        $settings = array(
            'oauth_access_token' => env('OAUTH_ACCESS_TOKEN'),
            'oauth_access_token_secret' => env('OAUTH_ACCESS_TOKEN_SECRET'),
            'consumer_key' => env('CONSUMER_KEY'),
            'consumer_secret' => env('CONSUMER_SECRET')
        );

        $url = "https://api.twitter.com/1.1/search/tweets.json";

        $requestMethod = "GET";
        $getfield = '?q=%'.$text_input.'%&result_type=popular&count=1';
        $twitter = new TwitterAPIExchange($settings);
        $twitter_response = $twitter->setGetfield($getfield)
            ->buildOauth($url, $requestMethod)
            ->performRequest();

        $tweet = new Tweet(json_decode($twitter_response)->statuses[0]);

        $data = array(
            'name' => "Prova Inbenta",
            'text' => $tweet->text()
        );

        Mail::send('emails.sendTweet', $data, function ($message) use ($email) {

            $message->from('markanft@gmail.com', 'Marc Andrés');

            $message->to($email)->subject('Proba Inbenta Twitter');

        });

        echo $twitter_response;
    }

}

?>
