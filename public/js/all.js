
$( document ).ready(function() {
	// Handler for .ready() called.
	$('.btn-success').click(function(){
		text = $('#input').val();
		email = $('#email').val();
		token = $('#token').val();
		search_in_twitter(text, email);
	})
});

function search_in_twitter(text, email){
	$("#text_twitter").html("Loading...");
	$.post( "/action", { text: text, email: email, _token: token})
  	.done(function( data ) {
  		twitter = JSON.parse(data);
  		$("#text_twitter").html(twitter['statuses'][0]['text']);
  	});
}